#!/bin/bash

#reminder that 0 and 1 are opposite for true/false in bash
confirm_prompt ()
{
    if [[ $1 =~ ^([yY][eE][sS]|[yY])$ ]]
    then
        return 0
    else
        return 1
    fi
}

fast_setup ()
{
    if confirm_prompt $should_prompt
    then
        read -r -p "$1? [y/N] " prompt
        return $(confirm_prompt $prompt)
    else
        return 0
    fi
}

#-------------------------------
#-------SETUP-------------------
#-------------------------------

echo "You will probably want another computer next to you to jot down passwords."
echo -e "This won't set up browser plugins like Lastpass.\n"

read -r -p "Hit enter to begin"
#sudo apt-get update -y


#-------------------------------
#--------SOFTWARE---------------
#-------------------------------
read -r -p "Install Software? " section
if confirm_prompt $section; then
    read -r -p "Prompt before each software installation? [y/N] " should_prompt

    #-----------------------------------------
    # Git (needed later on in the list)
    #-----------------------------------------

    if fast_setup "Install git"; then
        sudo apt-get install -y git
        sudo apt install -y git-gui
    fi

    read -r -p "Add ssh key to github? [y/N] " prompt
    if confirm_prompt $prompt; then
        ssh-keygen -t rsa -b 4096 -C "shumway.danny@gmail.com"
        eval "$(ssh-agent -s)"
        ssh-add ~/.ssh/id_rsa
        curl -u "shumway.danny@gmail.com" \
             --data "{\"title\":\"linux-home\",\"key\":\"`cat ~/.ssh/id_rsa.pub`\"}" \
             https://api.github.com/user/keys
    fi

    ## @todo Add SSH key to Gitlab

    #-------------------------------------------
    # NPM (also a pre-req)
    #-------------------------------------------
    if fast_setup 'Install node (nvm)'; then
        curl -o- https://raw.githubusercontent.com/creationix/nvm/master/install.sh | bash
        [ -s "~/.nvm/nvm.sh" ] && . "~/.nvm/nvm.sh"  # load nvm into current terminal
        nvm install node
    fi

    #-------------------------------------------
    # Emacs (Spacemacs)
    #-------------------------------------------
    if fast_setup 'Install Emacs (Spacemacs)'; then
        sudo apt install -y emacs
        git clone https://github.com/syl20bnr/spacemacs ~/.emacs.d
    fi

    read -r -p "Download your Spacemacs config? (uses ~/Code/config/spacemacs) [y/N] " prompt
    if confirm_prompt $prompt; then
        #If directory already exists, then I trust you to have set it up correctly
        if [ ! -d "$~/Code/config/spacemacs" ]; then
            mkdir ~/Code/config/spacemacs
            git clone git@gitlab.com:danShumway/spacemacs.git ~/Code/config/spacemacs
        fi

        cd ~/Code/config/.spacemacs && git checkout linux
        npm run link
    fi

    #--------------------------------------------
    # Dropbox
    #--------------------------------------------
    if fast_setup 'Install Dropbox'; then
        sudo add-apt-repository 'deb http://linux.dropbox.com/ubuntu xenial main'
        sudo apt-key adv --keyserver pgp.mit.edu --recv-keys 1C61A2656FB57B7E4DE0F4C1FC918B335044912E
        sudo apt-get update
        sudo apt install -y dropbox

        echo " - You will need to run dropbox at least once to begin using it"
    fi

    #--------------------------------------------
    # Redis
    #--------------------------------------------
    if fast_setup 'Install Redis'; then
        sudo apt install -y build-essential tcl

        cd /tmp/
        curl -O http://download.redis.io/redis-stable.tar.gz
        tar xzvf redis-stable.tar.gz
        cd /tmp/redis-stable
        make
        sudo make install

        sudo mkdir /etc/redis
        sudo cp /tmp/redis-stable/redis.conf /etc/redis

        echo " - See https://www.digitalocean.com/community/tutorials/how-to-install-and-configure-redis-on-ubuntu-16-04 for more configuration steps"
    fi

    #--------------------------------------------
    # Asciidoc & extras
    #--------------------------------------------
    if fast_setup 'Install AsciiDoc (+ extras)'; then
        sudo apt install -y asciidoctor

        cd /tmp/
        git clone https://github.com/asciidoctor/asciidoctor-epub3
        cd asciidoctor-epub3
        sudo apt install -y build-essential patch ruby-dev zlib1g-dev liblzma-dev
        sudo apt install -y ruby-bundler
        bundle install
        rake build
        sudo gem install pkg/asciidoctor-epub3*
        sudo gem install coderay
        sudo gem install pygments.rb
    fi

    #--------------------------------------------
    # Callibre
    #--------------------------------------------
    if fast_setup 'Install Calibre'; then
        sudo -v && wget -nv -O- https://download.calibre-ebook.com/linux-installer.py | sudo python -c "import sys; main=lambda:sys.stderr.write('Download failed\n'); exec(sys.stdin.read()); main()"
    fi

    #--------------------------------------------
    # IPFS
    #--------------------------------------------
    if fast_setup 'Install IPFS'; then
        ipfs_version=$(curl -s https://dist.ipfs.io/ipfs-update/versions | sed '$!d')
        echo ${ipfs_version}

        cd /tmp/
        wget https://dist.ipfs.io/ipfs-update/${ipfs_version}/ipfs-update_${ipfs_version}_linux-amd64.tar.gz
        tar xvfz ipfs-update*.tar.gz
        sudo mv ipfs-update/ipfs-update /usr/local/bin/ipfs-update
        sudo ipfs-update install latest
    fi

    #--------------------------------------------
    # Misc
    #--------------------------------------------
    if fast_setup 'Install Misc (multiple packages)'; then
        sudo apt install -y vlc
        sudo apt install -y ubuntu-restricted-extras
        sudo apt install -y libdvdread4
        sudo apt autoremove -y #sometimes necessary to get it using the correct drivers

        sudo apt install libgcrypt #Necessary for ssh support in some node packages
        sudo apt install libssl-dev

        sudo apt install libaacs0 libbluray1 libbluray-bdj #blu-ray support
        mkdir -p ~/.config/aacs
        cd ~/.config/aacs/ && wget http://vlc-bluray.whoknowsmy.name/files/KEYDB.cfg


        sudo apt install -y audacity
        sudo apt install -y lolcat
    fi

    #--------------------------------------------
    # Disabled (until I confirm I want them)
    #--------------------------------------------
    # sudo apt install -y tracker
    # sudo apt install -y tracker-gui

    #--------------------------------------------
    # Opera - you can safely answer yes or no when prompted for auto-update
    #--------------------------------------------
    # if fast_setup 'Install Opera'; then
    #     sudo add-apt-repository 'deb https://deb.opera.com/opera-stable/ stable non-free'
    #     wget -q0- https://deb.opera.com/archive.key | sudo apt-key add -
    #     sudo apt-get update -y
    #     sudo apt-get install -y opera-stable
    # fi

    #--------------------------------------------
    # Nuova Player (Google Play)
    #--------------------------------------------
    # if fast_setup 'Install Nuova Player (Google Play)'; then
    #     read -r -s -p "Enter your tiliado.eu password: " psswrd
    #     response=$(curl --data-urlencode "username=danShumway" \
    #                     --data-urlencode "password=$psswrd" \
    #                     --data "scope=default" https://tiliado.eu/api-auth/obtain-token/)

    #     token=$(echo $response | sed 's/^.*"token": "\([^"]*\)".*$/\1/') #parse out the json
    #     sudo add-apt-repository "deb https://danShumway:$token@tiliado.eu/nuvolaplayer/repository/deb/ xenial devel"
    #     sudo apt-key adv --keyserver hkp://keyserver.ubuntu.com:80 --recv-keys 40554B8FA5FE6F6A
    #     sudo apt-get update
    #     sudo apt install -y nuvolaplayer3-google-play-music
    # fi
fi

#-------------------------------
#-------SYSTEM SETTINGS---------
#-------------------------------
read -r -p "Prompt per setting? [y/N] " should_prompt

#--------------------------------------------
# Unity settings
#--------------------------------------------
if fast_setup 'Configure destkop'; then
    gsettings set org.gnome.desktop.background show-desktop-icons false
    gsettings set org.gnome.desktop.background picture-uri 'file:///usr/share/backgrounds/TCP118v1_by_Tiziano_Consonni.jpg'
fi

if fast_setup 'Configure application menus'; then
    gsettings set com.canonical.Unity integrated-menus true
    gsettings set com.canonical.Unity always-show-menus true
fi

#--------------------------------------------
# Dock/Nautilus
#--------------------------------------------

#-------------------------------
#-------BACKUP------------------
#-------------------------------
